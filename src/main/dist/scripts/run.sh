#!/bin/bash
#
# Script que executa um módulo xctl
#
WORKSPACE_PATH=$(pwd)
XTOOL_DESCRIPTOR="$WORKSPACE_PATH/xtool.yaml"
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RESET_COLOR="\033[0m"

# Verifica se o descritor xtool.yaml existe. Em caso negativo a execução do script é interrompida.
if [[ ! -e "$XTOOL_DESCRIPTOR" ]]; then
  echo -e "${RED_COLOR}[ERROR]${RESET_COLOR} Não foi possível encontrar o arquivo descritor xtool.yaml em $WORKSPACE_PATH"
  exit 1
fi

MODULE_NAME=$(yq '.module.name' <"$XTOOL_DESCRIPTOR")
MODULE_VERSION=$(yq '.module.version' <"$XTOOL_DESCRIPTOR")
MODULE_LOCATION=$(yq '.module.location' <"$XTOOL_DESCRIPTOR")

# Verifica se a variável MODULE_LOCATION está definida no arquivo descritor xtool.yaml
# o que irá indicar uma execução direta pelo jar
if [[ -f "$MODULE_LOCATION" ]]; then
  echo -e "${CYAN_COLOR}[INFO ]${RESET_COLOR} Executando jar diretamente em: $MODULE_LOCATION"
  java -jar "$MODULE_LOCATION" run \
    --spring.config.location=file:"$WORKSPACE_PATH"/xtool.yaml
  exit 0
fi

MODULE_JAR="$XTOOL_HOME"/modules/"$MODULE_NAME"/"$MODULE_VERSION"/"$MODULE_NAME"-"$MODULE_VERSION".jar
# Executa o módulo baseado no nome é versão definidos no arquivo descritor xtool.yaml
java -jar "$MODULE_JAR" run \
  --spring.config.location=file:"$WORKSPACE_PATH"/xtool.yaml
