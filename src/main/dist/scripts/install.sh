#!/bin/bash
#
# Script responsável por instalar um novo módulo xctl
#
XTOOL_HOME="$HOME/.xtool"
#BOLD="\033[1m"
#CYAN_COLOR="\033[96m"
#RED_COLOR="\033[91m"
#RESET_COLOR="\033[0m"
#
#MODULE_NAME=$1
#MODULE_BINARY="$MODULE_NAME".zip
#GITLAB_API_URL="https://gitlab.com/api/v4"
#GITLAB_XMODULES_GROUP_ID="67015724"
#GITLAB_PROJECT_ID=$(curl -s $GITLAB_API_URL/groups/$GITLAB_XMODULES_GROUP_ID/projects | jq ".[] | select(.name == \"$MODULE_NAME\")" | jq '.id')
#GITLAB_PROJECT_RELEASE_URL="$GITLAB_API_URL/projects/$GITLAB_PROJECT_ID/releases/"
#GITLAB_PROJECT_RELEASE_VERSION=$(curl -s "$GITLAB_PROJECT_RELEASE_URL" | jq '.[]' | jq -r '.name' | head -1 | cut -d' ' -f2)
#GITLAB_PROJECT_GENERIC_PGK_BASE_URL="$GITLAB_API_URL/projects/$GITLAB_PROJECT_ID/packages/generic/$MODULE_NAME"
#
#curl -# -0 "$GITLAB_PROJECT_GENERIC_PGK_BASE_URL"/"$GITLAB_PROJECT_RELEASE_VERSION"/"$MODULE_BINARY" -o /tmp/"$MODULE_BINARY"
#
##rm "$XTOOL_HOME"/modules/"$MODULE_NAME"* >/dev/null
#
#cd /tmp
#rm -rf /tmp/"$MODULE_NAME"/
#unzip /tmp/"$MODULE_BINARY" >/dev/null
#mv /tmp/"$MODULE_NAME"/"$MODULE_NAME"-"$GITLAB_PROJECT_RELEASE_VERSION".jar "$XTOOL_HOME"/modules/"$GITLAB_PROJECT_RELEASE_VERSION"
#mv /tmp/"$MODULE_NAME"/metadata.yaml "$XTOOL_HOME"/modules/"$GITLAB_PROJECT_RELEASE_VERSION"
#
#echo "Versão $GITLAB_PROJECT_RELEASE_VERSION do módulo $MODULE_NAME instalado com sucesso!"
