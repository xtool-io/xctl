#!/bin/bash
#
# Script que lista todos os módulos xctl instalados
#
#set -e

XTOOL_HOME="$HOME/.xtool"
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RED_COLOR="\033[91m"
RESET_COLOR="\033[0m"

find "$XTOOL_HOME"/modules -type f -name "*.yaml" -print0 | while read -d $'\0' file; do
  MODULE_NAME=$(yq '.name' <"$file")
  MODULE_DESCRIPTION=$(yq '.description' <"$file")
  echo -e "${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} - $MODULE_DESCRIPTION"
done
