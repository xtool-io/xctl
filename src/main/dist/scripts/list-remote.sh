#!/bin/bash
#
# Script que lista todos os módulos xctl disponíveis no repositório remoto
#
#set -e

XTOOL_HOME="$HOME/.xtool"
#BOLD="\033[1m"
#CYAN_COLOR="\033[96m"
#RED_COLOR="\033[91m"
#RESET_COLOR="\033[0m"

XMODULES_GROUP_ID="67015724"

XMODULES=$(curl  -s https://gitlab.com/api/v4/groups/$XMODULES_GROUP_ID/projects | jq -r '.[].name + " -  " + .[].description')

echo $XMODULES

