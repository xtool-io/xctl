#!/bin/bash
#
# Script responsável pela execução do initializer do módulo
#
MODULE=$1
XTOOL_HOME="$HOME"/.xtool
WORKSPACE_PATH=$(pwd)
RED_COLOR="\033[91m"
RESET_COLOR="\033[0m"

# Valida se o workspace está vazio. Em caso negativo a execução do script é interrompida.
function validateWorkspaceIsEmpty() {
  if [ -n "$(find "$WORKSPACE_PATH" -maxdepth 1 -not -empty)" ]; then
    echo -e "${RED_COLOR}[ERROR]${RESET_COLOR} Para a execução do comando de inicialização o diretório atual deve estar vazio."
    exit 1
  fi
}

# Valida se o jar existe. Em caso negativo a execução do script é interrompida.
function validateJarExists() {
  if [[ ! -f "$MODULE" ]]; then
    echo -e "${RED_COLOR}[ERROR]${RESET_COLOR} Não foi possível localizar o jar do módulo em: $MODULE"
    exit 1
  fi
}

# Verificar se é uma inicialização direta via jar
if [[ -f "$MODULE" && "$MODULE" == *.jar ]]; then
  validateWorkspaceIsEmpty
  validateJarExists
  shift
  java -jar "$MODULE" init \
    "$@" \
    --spring.main.banner-mode=off
  # Adiciona a chave module.location ao arquivo descritor xtool.yaml
  # informando o caminho direto para o módulo
  yq eval ".module.location = \"$MODULE\"" -i "$WORKSPACE_PATH"/xtool.yaml
  exit 0
fi

## Verifica se o diretório do módulo existe em $XTOOL_HOME/modules
#if [[ ! -d "$XTOOL_HOME"/modules/"$MODULE_NAME" ]]; then
#  echo -e "${RED_COLOR}[ERROR]${RESET_COLOR} Não foi possível encontrar o módulo $MODULE_NAME"
#  exit 1
#fi
#
## Verifica se o diretório de trabalho está vazio
#if [ -n "$(find "$WORKSPACE_PATH" -maxdepth 1 -not -empty)" ]; then
#  echo -e "${RED_COLOR}[ERROR]${RESET_COLOR} Para a execução do comando de inicialização o diretório atual deve estar vazio."
#  exit 1
#fi
#
#shift
#MODULE_METADATA="$XTOOL_HOME"/modules/"$MODULE_NAME"/metadata.yaml
#MODULE_ARGS=$1
#MODULE_VERSION=$(yq '.version' <"$MODULE_METADATA")
#MODULE_JAR="$XTOOL_HOME"/modules/"$MODULE_NAME"/"$MODULE_VERSION"/"$MODULE_NAME"-"$MODULE_VERSION".jar
#
#if [[ ! -f "$MODULE_JAR" ]]; then
#  echo -e "${RED_COLOR}[ERROR]${RESET_COLOR} Não foi possível localizar o jar do módulo em: $MODULE_JAR"
#  exit 1
#fi

java -jar "$MODULE_JAR" init \
  "$MODULE_ARGS" \
  --spring.main.banner-mode=off
