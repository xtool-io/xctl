package xtool.xctl.command

import org.springframework.stereotype.Component
import picocli.CommandLine.Command

@Component
@Command(name = "", subcommands = [InitCommand::class, VersionCommand::class])
class RootCommand : Runnable {

    override fun run() {

    }
}
