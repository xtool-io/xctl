package xtool.xctl.command

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import picocli.CommandLine.Command

@Component
@Command(name = "version")
class VersionCommand: Runnable {

    private val log = LoggerFactory.getLogger(RootCommand::class.java)
    override fun run() {
        log.info("version.....")
    }
}
