package xtool.xctl.command

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import picocli.CommandLine.Command

@Component
@Command(name = "init")
class InitCommand : Runnable {

    private val log = LoggerFactory.getLogger(RootCommand::class.java)
    override fun run() {
        log.info("Init.....")
    }
}
