package xtool.xctl

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.ExitCodeGenerator
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import picocli.CommandLine
import picocli.CommandLine.IFactory
import xtool.xctl.command.RootCommand

@SpringBootApplication
class XctlApplication(
    private var factory: IFactory,
    private var rootCommand: RootCommand
) : CommandLineRunner, ExitCodeGenerator {

    private var exitCode = 0

    override fun run(vararg args: String) {
        // let picocli parse command line args and run the business logic
        exitCode = CommandLine(rootCommand, factory).execute(*args)
    }

    override fun getExitCode(): Int {
        return exitCode
    }

}

fun main(args: Array<String>) {
    runApplication<XctlApplication>(*args)
}
