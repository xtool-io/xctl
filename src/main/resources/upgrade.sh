#!/bin/bash

set -e

XTOOL_RELEASES_URL="https://gitlab.com/api/v4/projects/37376658/releases/"

XTOOL_GENERIC_PGK_BASE_URL="https://gitlab.com/api/v4/projects/37376658/packages/generic/xtool"

XTOOL_BINARY="xctl.zip"

XTOOL_HOME="$HOME/.xtool"

XTOOL_RELEASE_VERSION=$(curl -s $XTOOL_RELEASES_URL | jq '.[]' | jq -r '.name' | head -1 | cut -d' ' -f2)

LOCAL_VERSION=$(cat $XTOOL_HOME/VERSION)

if [[ $XTOOL_RELEASE_VERSION == $LOCAL_VERSION ]]; then
  echo "Não há atualizações dispoíveis."
  exit 0
fi

echo ""
echo "Atualizando ferramenta xtool para $XTOOL_RELEASE_VERSION. Aguarde..."

curl -# -0 $XTOOL_GENERIC_PGK_BASE_URL/"$XTOOL_RELEASE_VERSION"/$XTOOL_BINARY -o "$XTOOL_HOME"/tmp/$XTOOL_BINARY
cd "$XTOOL_HOME"/tmp/
rm -rf "$XTOOL_HOME"/bin/ "$XTOOL_HOME"/lib/ "$XTOOL_HOME"/scripts/
unzip -q $XTOOL_BINARY

cp xctl/bin/xctl "$XTOOL_HOME"/bin
cp -R xctl/lib/* "$XTOOL_HOME"/lib
cp -R xctl/scripts/* "$XTOOL_HOME"/scripts
rm -rf "$XTOOL_HOME"/tmp/*
echo "$XTOOL_RELEASE_VERSION" > "$XTOOL_HOME"/VERSION
