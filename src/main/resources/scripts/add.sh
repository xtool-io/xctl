#!/bin/bash

set -e

XTOOL_HOME="$HOME/.xtool"
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RED_COLOR="\033[91m"
RESET_COLOR="\033[0m"
MODULE_REPO=$1
MODULE_NAME=$(echo "$MODULE_REPO" | sed 's/.*\/\([^ ]*\.git\)/\1/;s/\.git$//')

if [ ! -d "$XTOOL_HOME"/modules/"$MODULE_NAME" ]; then
  echo "Baixando módulo. Aguarde..."
  cd "$XTOOL_HOME"/modules
  git clone -q "$MODULE_REPO"
  echo -e "Módulo ${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} adicionado com sucesso!"
  . "$XTOOL_HOME"/scripts/update.sh --index >/dev/null

  exit 0
fi

echo -e "Módulo já instalado."
