#!/bin/bash

set -e

XTOOL_HOME="$HOME/.xtool"
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RED_COLOR="\033[91m"
RESET_COLOR="\033[0m"

. "$XTOOL_HOME"/scripts/validate-yq.sh

# Caso a opção -s seja passada como argumento a listagem de submódulos também será exibida.
if [ -n "$1" ] && [ "$1" == "-s" ]; then
  PRINT_SUBMODULES=true
fi

print_submodules() {
  if [ -n "$PRINT_SUBMODULES" ]; then
    if [ -d "$1" ]; then
      local total_submodules=$(find "$1" -maxdepth 1 -type d | wc -l)
      local index=0
      for SUBMODULE_DIR in "$1"/*; do
        SUBMODULE_NAME=$(yq '.name' <"$SUBMODULE_DIR"/module.yml)
        SUBMODULE_DESCRIPTION=$(yq '.description' <"$SUBMODULE_DIR"/module.yml)
        if [ $index -eq $(($total_submodules - 2)) ]; then
          echo -e " └── ${BOLD}${CYAN_COLOR}${SUBMODULE_NAME}${RESET_COLOR} - $SUBMODULE_DESCRIPTION"
        else
          echo -e " ├── ${BOLD}${CYAN_COLOR}${SUBMODULE_NAME}${RESET_COLOR} - $SUBMODULE_DESCRIPTION"
        fi
        index=$((index + 1))
      done
    fi
  fi
}

for MODULE_DIR in "$XTOOL_HOME"/modules/*; do
  if [ -d "$MODULE_DIR" ]; then
    MODULE_NAME=$(yq '.name' <"$MODULE_DIR"/module.yml)
    MODULE_DESCRIPTION=$(yq '.description' <"$MODULE_DIR"/module.yml)
    UPDATE_ICON="\u2199"
    # Verifica se o repositório está versionado
    if git -C "$MODULE_DIR" rev-parse --is-inside-work-tree >/dev/null 2>&1; then
      # Verifica se há atualizações no repositório remoto
      git -C "$MODULE_DIR" fetch >/dev/null 2>&1
      if ! git -C "$MODULE_DIR" status -uno | grep -q "Your branch is up to date with"; then
        echo -e "${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} ${BOLD}${RED_COLOR}$UPDATE_ICON${RESET_COLOR} - $MODULE_DESCRIPTION"
        print_submodules "$MODULE_DIR"/submodules
        continue
      fi

    fi

    echo -e "${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} - $MODULE_DESCRIPTION"
    print_submodules "$MODULE_DIR"/submodules
  fi
done
