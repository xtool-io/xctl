#!/bin/bash

set -e

XTOOL_HOME="$HOME/.xtool"
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RESET_COLOR="\033[0m"

. "$XTOOL_HOME"/scripts/validate-yq.sh

for MODULE_DIR in "$XTOOL_HOME"/modules/*; do
  if [ -d "$MODULE_DIR" ]; then
    MODULE_NAME=$(yq '.name' <"$MODULE_DIR"/module.yml)
    MODULE_DESCRIPTION=$(yq '.description' <"$MODULE_DIR"/module.yml)
    if [ "$MODULE_NAME" == "$1" ]; then
      echo -e "Nome: ${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR}"
      echo -e "Descrição: ${BOLD}${CYAN_COLOR}${MODULE_DESCRIPTION}${RESET_COLOR}"
      REMOTE_REPO=$(git -C "$MODULE_DIR" remote get-url origin)
      echo -e "Repositório: ${BOLD}${CYAN_COLOR}${REMOTE_REPO}${RESET_COLOR}"
      SHORT_COMMIT_ID=$(git -C "$MODULE_DIR" rev-parse --short HEAD)
      DATE_COMMMIT_ID=$(git -C "$MODULE_DIR" log -1 --format="%cd" --date=format:'%d/%m/%Y %H:%M:%S')
      echo -e "Versão: ${BOLD}${CYAN_COLOR}${SHORT_COMMIT_ID} ($DATE_COMMMIT_ID) ${RESET_COLOR}"
      COMMITER=$(git -C "$MODULE_DIR" log -1 --format="%an <%ae>")
      echo -e "Commiter: ${BOLD}${CYAN_COLOR}${COMMITER}${RESET_COLOR}"
      exit 0
    fi
  fi
done
echo -e "Módulo ${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} não encontrado."
