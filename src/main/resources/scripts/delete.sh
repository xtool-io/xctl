#!/bin/bash

set -e

XTOOL_HOME="$HOME/.xtool"
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RESET_COLOR="\033[0m"

. "$XTOOL_HOME"/scripts/validate-yq.sh

for MODULE_DIR in "$XTOOL_HOME"/modules/*; do
  if [ -d "$MODULE_DIR" ]; then
    MODULE_NAME=$(yq '.name' <"$MODULE_DIR"/module.yml)
    if [ "$MODULE_NAME" == "$1" ]; then
      rm -rf "$MODULE_DIR"
      echo -e "Módulo ${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} removido com sucesso."
      . "$XTOOL_HOME"/scripts/update.sh --index >/dev/null
      exit 0
    fi
  fi
done
echo -e "Módulo ${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} não encontrado."
