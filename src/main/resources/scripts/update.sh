#!/bin/bash

set -e

XTOOL_HOME="$HOME/.xtool"
BOLD="\033[1m"
CYAN_COLOR="\033[96m"
RED_COLOR="\033[91m"
RESET_COLOR="\033[0m"

update_index() {
  local index_file="$XTOOL_HOME"/modules/.index
  if [ -e "$index_file" ]; then
    rm "$index_file"
  fi
  touch "$index_file"
  for MODULE_DIR in "$XTOOL_HOME"/modules/*; do
    if [ -d "$MODULE_DIR" ]; then
      MODULE_NAME=$(yq '.name' <"$MODULE_DIR"/module.yml)
      echo "$MODULE_NAME" >>"$index_file"
      if [ -d "$MODULE_DIR"/submodules/ ]; then
        for SUBMODULE_DIR in "$MODULE_DIR"/submodules/*; do
          SUBMODULE_NAME=$(yq '.name' <"$SUBMODULE_DIR"/module.yml)
          echo "${MODULE_NAME}@${SUBMODULE_NAME}" >>"$index_file"
        done
      fi
    fi
  done
  echo "Índice de módulos atualizado com sucesso."
}

# Caso a opção -s seja passada como argumento a listagem de submódulos também será exibida.
if [ -n "$1" ] && [ "$1" == "--index" ]; then
  update_index
  exit 0
fi

# Atualiza os módulos instalados

for MODULE_DIR in "$XTOOL_HOME"/modules/*; do
  if [ -d "$MODULE_DIR" ]; then
    MODULE_NAME=$(yq '.name' <"$MODULE_DIR"/module.yml)
    # Verifica se o repositório está versionado
    if git -C "$MODULE_DIR" rev-parse --is-inside-work-tree >/dev/null 2>&1; then
      # Verifica se há atualizações no repositório remoto
      git -C "$MODULE_DIR" fetch >/dev/null 2>&1
      if ! git -C "$MODULE_DIR" status -uno | grep -q "Your branch is up to date with"; then
        git -C "$MODULE_DIR" pull >/dev/null 2>&1
        echo -e "${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} - Atualizado"
        continue
      fi

    fi

    echo -e "${BOLD}${CYAN_COLOR}${MODULE_NAME}${RESET_COLOR} - Sem atualizações"
  fi
done
