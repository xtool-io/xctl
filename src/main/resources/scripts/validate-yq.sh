#!/bin/bash

if ! command -v yq >/dev/null; then
  echo "Ferramenta não encontrada."
  echo "======================================================================================================"
  echo " Por favor, instale a ferramenta yq com os procedimentos encontrado no endereço abaixo: "
  echo " MacOS: brew install yq"
  echo " Linux: wget https://github.com/mikefarah/yq/releases/download/v4.31.1/yq_linux_amd64.tar.gz -O - | tar xz && sudo mv yq_linux_amd64 /usr/bin/yq"
  echo ""
  echo " Após a instalação repita a operação."
  echo "======================================================================================================"
  echo ""
  exit 1
fi

