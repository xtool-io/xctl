#!/bin/bash

set -e

echo "Procurando pela ferramenta jq..."
if ! command -v jq > /dev/null; then
	echo "Ferramenta não encontrada."
	echo "======================================================================================================"
	echo " Por favor, instale a ferramenta jq com os procedimentos encontrado no endereço abaixo: "
	echo " https://stedolan.github.io/jq/download/"
	echo ""
	echo " Após a instalação repita a operação."
	echo "======================================================================================================"
	echo ""
	exit 1
fi

echo "Procurando pela ferramenta yq..."
if ! command -v yq > /dev/null; then
	echo "Ferramenta não encontrada."
	echo "======================================================================================================"
	echo " Por favor, instale a ferramenta yq com os procedimentos encontrado no endereço abaixo: "
	echo " MacOS: brew install yq"
	echo " Linux: wget https://github.com/mikefarah/yq/releases/download/v4.31.1/yq_linux_amd64.tar.gz -O - | tar xz && sudo mv yq_linux_amd64 /usr/bin/yq"
	echo ""
	echo " Após a instalação repita a operação."
	echo "======================================================================================================"
	echo ""
	exit 1
fi

echo "Procurando pela ferramenta unzip..."
if ! command -v unzip > /dev/null; then
	echo "Ferramenta não encontrada."
	echo "======================================================================================================"
	echo " Por favor, instale a ferramenta unzip com o seu gerenciador de pacotes "
	echo ""
	echo " Após a instalação repita a operação."
	echo "======================================================================================================"
	echo ""
	exit 1
fi

echo "Procurando pelo java..."
if ! command -v java > /dev/null; then
	echo "Ferramenta não encontrada."
	echo "======================================================================================================"
	echo " Por favor, instale o java com o utilitário sdk (https://sdkman.io/): "
	echo ""
	echo "$ sdk install java 8.0.302-open"
	echo ""
	echo " Após a instalação repita a operação."
	echo "======================================================================================================"
	echo ""
	exit 1
fi

echo "Procurando pelo git..."
if ! command -v git > /dev/null; then
	echo "Ferramenta não encontrada."
	echo "======================================================================================================"
	echo " Por favor, instale o git com o seu gerenciador de pacotes."
	echo ""
	echo " Após a instalação repita a operação."
	echo "======================================================================================================"
	echo ""
	exit 1
fi

echo "Procurando pela ferramenta maven..."
if ! command -v mvn > /dev/null; then
	echo "Ferramenta não encontrada."
	echo "======================================================================================================"
	echo " Por favor, instale a ferramenta maven com o utilitário sdk (https://sdkman.io/): "
	echo ""
	echo "$ sdk install maven"
	echo ""
	echo " Após a instalação repita a operação."
	echo "======================================================================================================"
	echo ""
	exit 1
fi

XTOOL_RELEASES_URL="https://gitlab.com/api/v4/projects/37376658/releases/"

XTOOL_GENERIC_PGK_BASE_URL="https://gitlab.com/api/v4/projects/37376658/packages/generic/xtool"

XTOOL_BINARY="xctl.zip"

XTOOL_HOME="$HOME/.xtool"

# XTOOL_RELEASE_VERSION=$(curl -s $XTOOL_RELEASES_URL | jq '.[]' | jq -r '.name' | head -1 | cut -d' ' -f2)

XTOOL_RELEASE_VERSION="v1.13.0"

mkdir -p "$XTOOL_HOME"/bin
mkdir -p "$XTOOL_HOME"/modules
mkdir -p "$XTOOL_HOME"/scripts
#touch "$XTOOL_HOME"/VERSION

# OS specific support (must be 'true' or 'false').
cygwin=false
darwin=false
solaris=false
freebsd=false
case "$(uname)" in
CYGWIN*)
  cygwin=true
  ;;
Darwin*)
  darwin=true
  ;;
SunOS*)
  solaris=true
  ;;
FreeBSD*)
  freebsd=true
  ;;
esac

xtool_bash_profile="${HOME}/.bash_profile"
xtool_profile="${HOME}/.profile"
xtool_bashrc="${HOME}/.bashrc"

echo "Baixando versão $XTOOL_RELEASE_VERSION"

curl -# -0 $XTOOL_GENERIC_PGK_BASE_URL/"$XTOOL_RELEASE_VERSION"/$XTOOL_BINARY -o /tmp/$XTOOL_BINARY
cd /tmp/
unzip -q $XTOOL_BINARY
cp xctl/bin/xctl "$XTOOL_HOME"/bin
cp -R xctl/scripts/* "$XTOOL_HOME"/scripts
cp xctl/metadata.yaml "$XTOOL_HOME"/metadata.yaml
#cp -R xctl/lib/* "$XTOOL_HOME"/lib
#cp xctl/upgrade.sh "$XTOOL_HOME"
#chmod +x "$XTOOL_HOME"/upgrade.sh
#rm -rf "$XTOOL_HOME"/tmp/*
#echo "$XTOOL_RELEASE_VERSION" > "$XTOOL_HOME"/VERSION


xtool_init_snippet=$(
  cat <<EOF
export XTOOL_HOME="$HOME/.xtool"
export PATH="$PATH:$XTOOL_HOME/bin"
EOF
)

if [[ $darwin == true ]]; then
  touch "$xtool_bash_profile"
#  echo "Tentando atualizar bash profile no OSX..."
  if [[ -z $(grep 'XTOOL_HOME' "$xtool_bash_profile") ]]; then
    echo -e "\n$xtool_init_snippet" >>"$xtool_bash_profile"
#    echo "Snippet de inicialização do xtool adicionado em $xtool_bash_profile"
  fi
else
#  echo "Tentando atualizar bash profile..."
  touch "${xtool_bashrc}"
  if [[ -z $(grep 'XTOOL_HOME' "$xtool_bashrc") ]]; then
    echo -e "\n$xtool_init_snippet" >>"$xtool_bashrc"
#    echo "Snippet de inicialização do xtool adicionado em $xtool_bashrc"
  fi
fi

#echo "                               "
#echo "          ---------------------"
#echo "          |   HELLO WORLD!    |"
#echo "          | SEEK KNOWLEDGE!   |"
#echo "          |/-------------------"
#echo "          /                    "
#echo "      .--.   |V|               "
#echo "     /    \ _| /               "
#echo "     q .. p \ /                "
#echo "      \--/  //                 "
#echo "     __||__//                  "
#echo "    /.    _/                   "
#echo "   // \  /                     "
#echo "  //   ||                      "
#echo "  \\   /  \                     "
#echo "   )\|    |                    "
#echo "  / || || |                    "
#echo "  |/\| || |                    "
#echo "     | || |                    "
#echo "     \ || /                    "
#echo "   __/ || \__                  "
#echo "  \____/\____/                 "
#echo "                               "

echo -e "Instalação finalizada com sucesso! Por favor, abra e feche o terminal para iniciar o uso da ferramenta ou execute o comando:"
if [[ $darwin == true ]]; then
  echo -e "$ source ~/.bash_profile"
else
  echo -e "$ source ~/.bashrc"
fi

