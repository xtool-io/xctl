# xctl

## Instalação

```shell
$ curl -sL https://gitlab.com/xtool-io/xctl/-/raw/master/installer.sh | bash
```

## Modo desenvolvedor

```shell
$ mvn clean install -P devMode
```
